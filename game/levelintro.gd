extends Control


# Called when the node enters the scene tree for the first time.
func _ready():
	global.disabled=true
	pass # Replace with function body.


func _on_shower_pressed():
	global.disabled=true
	$ColorRect.show()
	$shower.hide()
	pass # Replace with function body.


func _on_hider_pressed():
	global.disabled=false
	$ColorRect.hide()
	$shower.show()
	pass # Replace with function body.
