extends Control


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	global.disabled=true
	pass # Replace with function body.


func _input(event):
	if Input.is_key_pressed(KEY_ESCAPE) and !global.writing:
		get_tree().paused=true
		self.show()


func _on_home_pressed():
	get_tree().paused=false
	get_tree().change_scene("res://Dissertation.tscn")
	pass # Replace with function body.


func _on_return_pressed():
	get_tree().paused=false
	self.hide()
	pass # Replace with function body.
