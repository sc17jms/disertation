extends Area2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"


func _on_Area2D_body_entered(_body):
	#print(body)
	#print("increment a variable length 10 times in a for loop")
	$Message.show()
	global.answer[0]="length"
	global.answer[1]=10
	pass

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func _input(_delta):
	if global.disabled==true:
		$Message/ableLabel.hide()
		$Message/disableLabel.show()
	else:
		$Message/ableLabel.show()
		$Message/disableLabel.hide()
	
		
	pass

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_Area2D_body_exited(_body):
	global.answer=["",""]

	$Message.hide()
	pass # Replace with function body.
