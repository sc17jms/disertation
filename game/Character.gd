extends KinematicBody2D

const WALK_SPEED = 200

var velocity = Vector2()


func _physics_process(delta):
	
	#if HUD showing don't move.
	if global.disabled==true:
		return
	
	
	if velocity.y<500 and !is_on_floor():
		velocity.y += delta * global.gravity *4
	elif is_on_floor(): #stops forever increasing gravity.
		velocity.y=0
	
	if Input.is_action_pressed("ui_left") or Input.is_key_pressed(KEY_A):
		velocity.x = -WALK_SPEED
	elif Input.is_action_pressed("ui_right")or Input.is_key_pressed(KEY_D):
		velocity.x =  WALK_SPEED
	else:
		velocity.x = 0
		
	if Input.is_action_pressed("ui_up") or Input.is_key_pressed(KEY_W):
		if is_on_floor():
			velocity.y=-WALK_SPEED *2
		#print("no rotate")
	elif Input.is_action_pressed("ui_down") or Input.is_key_pressed(KEY_S):
		#set downwards velocity and change rotation of the collision shape so that it falls through objects.
		#print("rotate")
		if velocity.y<350:
			velocity.y=350
	
	#less than and greater than so that when it equals 0 it stays the same way it was facing.
	#as the feet aren't perfectly centred we have to use a differen't hit box for left and right facing feet.
	if velocity.x<0:
		$Sprite.flip_h=true
		$Left.disabled=false
		$Right.disabled=true
	elif velocity.x>0:
		$Sprite.flip_h=false
		$Left.disabled=true
		$Right.disabled=false
	
	
	
	if global.answer!=["",""] and Input.is_key_pressed(KEY_E):
		global.disabled=true

	#$Sprite.flip_h=!$Sprite.flip_h
	
	
	# We don't need to multiply velocity by delta because "move_and_slide" already takes delta time into account.

	# The second parameter of "move_and_slide" is the normal pointing up.
	# In the case of a 2D platformer, in Godot, upward is negative y, which translates to -1 as a normal.
	move_and_slide(velocity, Vector2(0, -1))
	

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
