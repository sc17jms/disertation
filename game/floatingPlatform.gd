extends StaticBody2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

func _physics_process(_event):
	
	if Input.is_action_pressed("ui_down") or Input.is_key_pressed(KEY_S):
		#set downwards velocity and change rotation of the collision shape so that it falls through objects.
		#$CollisionShape2D.disabled(true)
		$CollisionShape2D.set_disabled(true)
	else:
		$CollisionShape2D.set_disabled(false)
	

	# We don't need to multiply velocity by delta because "move_and_slide" already takes delta time into account.

	# The second parameter of "move_and_slide" is the normal pointing up.
	# In the case of a 2D platformer, in Godot, upward is negative y, which translates to -1 as a normal.



# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
