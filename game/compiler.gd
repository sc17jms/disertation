extends Node

#Josh Scott dissertation project
#programming platformer game
#each function will return true, false, numeric value, or error.
#conditionals will be used. code will be created. world variables will be given and unchangable by the user.
#chacking value of these world value will be how we determine if an answer is correct or not
#all variables will be kept in a 2d array - maybe 3d if we move onto defining functions... 


######################################################################################

# to do list:

# make the in the brackets bit of for and if be allowed to be single boolean values too...?
# make game happen
# make the function calls made in sprint 2 be invoked by overlapping hit boxes in the game and pressing 'e'

# - change buttons so that if the player model is overlapping with a terminal it does the same thing - only when making game next sprint.
# - make a label that explains the code expected?
# - make while loops not work so that they don't break the game...
#	- special while variable?????????

#when looping over variables make sure to also give type cos it's useful for making looping over variable work properly for arrays.

#####
# sprint 3 plan
# make platformer. start with squares.
# make platforms and stuff
# make obstacles that will block path.

################################################################################################

# Called when the node enters the scene tree for the first time.
#init function basically
func _ready():
	formatting()
	global.vars = []
	global.gravity=200
	global.correct =["type",false]
	pass # Replace with function body.

func _input(_delta):
	if global.answer!=["",""] and Input.is_key_pressed(KEY_E):
		self.visible=true
		global.writing=true
		match global.answer[0]:
			"weight":
				$Banner/Label.text = "set a new variable weight to 4\n"
			"length":
				$Banner/Label.text = "increment a variable length 10 times in a for loop\n"
			"check":
				$Banner/Label.text = "Use an if statement to ensure that check is always true\n"
				if $CodeText.text=="":
					$CodeText.text="check=False\n"
	elif Input.is_key_pressed(KEY_ESCAPE) and global.writing:
		global.disabled=false
		self.visible=false
		global.writing=false

func formatting():
	#handles keywords in colours similar to python idle defaults.
	$CodeText.add_keyword_color("for",Color8(255,160,128))
	$CodeText.add_keyword_color("while",Color8(255,160,128))
	$CodeText.add_keyword_color("if",Color8(255,160,128))
	$CodeText.add_keyword_color("elif",Color8(255,160,128))
	$CodeText.add_keyword_color("else",Color8(255,160,128))
	
	$CodeText.add_keyword_color("in",Color8(255,128,255))
	$CodeText.add_keyword_color("range",Color8(255,128,255))
	
	#handles text inside of quotes either single or double like a text editor.
	$CodeText.add_color_region('"', '"', Color8(128,255,128),false)
	$CodeText.add_color_region("'", "'", Color8(128,255,128),false)
	pass


#range
func _range(var text):
	#print("range")
	var stripped = text.replace("range","").replace("(","").replace(")","").replace(":","")
	for i in global.vars:
		if i[0]==stripped:
			return range(i[1])
	
	return range(float(stripped))
	pass


#for
func _for(var text):
	#need to check for a conditional 'in'
	#python doesn't allow for chaining new vars in for loops.
	var fortext
	# ensures that the keyword 'in' is present and that there is a word after in that end in a :
	if 'in' in text[0] and text[0][-1][-1]==':': 
		print("syntax correct")
		#store the depth of the loop so that it loops over only those with lower depth
		var depth = text[0][0] 
		
		fortext=String(Array(text[0]).slice(1,-1)).replace(",","").replace("for","").replace("[","").replace("]","").replace(":","").strip_edges().split(" in ")
		
		for i in fortext:
			i.replace(" ","")
		
		# the below code will take the variables and plug them into the two halfs of the for loop so that i can make a real for loop using these functions.
		var forvariable = fortext[0]
		var forto = fortext[1]
		
		#chacks all defined variables in the array for the name used in the loop def.
		for i in global.vars:
			if i[0] == forvariable:
				forvariable=i[1]
			elif i[0]==forto:
				forto=i[1]
		
		if "range" in forto:
			 # will remove the range and brackets and colons to leave only the range.
			forto=_range(forto)
		elif "[" in forto:
			forto=Array(forto)
		elif '"' or "'" in forto:
			forto=String(forto)
		
		var inloop=0
		for i in text.slice(1,-1):
			if i[0]>depth:
				inloop+=1
			else:
				break
		 
		#set the default values incase an alternative was not found.
		if String(forvariable)==String(fortext[0]):
			#add variable to the vars array.
			global.vars.append([forvariable,forto[0],"undefined"])
		
		if inloop!=0:
			for forvariable in forto: 
				#only loop those that are at a higher depth than this one.
				for i in global.vars:
					if i[0]==fortext[0]:
						i[1]=forvariable
				loop(Array(text.slice(1,inloop)))
		else: 
			$ColorRect/CodeOutput.text +="syntax error. Are you using the 'in' keyword and ending the for string declaration with a colon?\n"
			return false
		
		#ensures that the last line isn't inside the loop or it will complete it one more time than the loop
		if text.size()>inloop+1:
			loop(Array(text.slice(inloop+1,-1)))
		
	else:
		$ColorRect/CodeOutput.text += "syntax error. Are you using the 'in' keyword and ending the for declaration with a colon?\n"
		return false
	return true
	#print(text)
	pass


#while
func _while(var text):
	$ColorRect/CodeOutput.text += "while loops can be dangerous and are not used in this program\n"
	print(text)
	pass


#if
func _if(var text, var found):
	print("if") # split the text input up into those if's that are at the same depth.
	
	# ensures that the conditional is present and that there is a line with higher depth proceeding it and ends with :
	if 'in'or'<'or'>'or'<='or'>='or'!='or'==' in text[0] and text[0][-1][-1]==':' and text[1][0]==text[0][0]+1:
		#text[0][-1][-1]=""
		var depth=text[0][0]
		#print("first",Array(text[0]).slice(1,-1))
		var iftext = String(Array(text[0]).slice(1,-1)).replace(",","").replace("elif","").replace("if","").replace("[","").replace("]","").replace(":","").replace(" in ","-/@in@/-").replace(" ","").strip_edges()
		
		if text.size()<=1:
			pass
		elif iftext =="else" and found==false:
			loop(Array(text.slice(1,-1)))
			return true
		
		iftext = iftext.replace("=="," == ").replace(">"," > ").replace("<"," < ").replace(" < ="," <= ").replace(" > ="," >= ").replace("!="," != ").replace("-/@in@/-"," in ")
		iftext = String(iftext).split(" ")
		
		if iftext.size()<3:
			$ColorRect/CodeOutput.text += "syntax error. Are you using a conditional argument and ending the if declaration with a colon?\n"
			return false
		
		var inif =0
		var nextif =0
		
		for x in text.slice(1,-1):
			if x[0]>depth:
				inif+=1
				continue
			elif int(x[0])==int(depth) and x[1]=='elif' or x[1]=='else:' or x[1]=='else':
				print("found future elif or else")
				nextif=inif+1
				break
			else:
				break
		
		if String(iftext)=="else" or String(iftext)=="else:" and not found:
			loop(text.slice(1,-1))
		
		#future project could set it up to be booleans so they can be combined with OR's and AND's
		
		var ifleft="LEFT ERROR"
		var ifright="RIGHT ERROR"
		if iftext.size()>=3:
			ifleft= iftext[0]
			ifright = iftext[2]
		else:
			return false
		
		if 'range' in ifright:
			ifright = _range(ifright)
			
		#### need to check both sides of the  conditional if they are infanct variables!!
		global.vars=Array(global.vars)
		iftext=Array(iftext)
		for i in global.vars:
			if iftext.size()>1:
				if i[0]==String(ifleft):
					ifleft=i[1]
				elif i[0]==String(ifright):
					ifright=i[1]
				
		
		# if next in line is elif or else then search for the next in line AFTER that.
		# need to slice at the point of the last else in line to skip it if we are meeting criteria.
		
		# if iftext tests possitive do the next lines then skip other ifs.
		# else if nextif != 0 then if(text slice nextif, true)
		# otherwise skip 
		
		if not found:
			
			#######
			#check that left and right are compatable types to check against.
			
			######### match ##############
			if iftext.size()<=2:
				pass
			
			match iftext[1]:
				"==":
					if String(ifleft)==String(ifright):
						found=true
						loop(Array(text.slice(1,inif)))
				"<=":
					if ifleft<=ifright:
						found=true
						loop(Array(text.slice(1,inif)))
				">=":
					if ifleft>=ifright:
						found=true
						loop(Array(text.slice(1,inif)))
				"<":
					if ifleft<ifright:
						found=true
						loop(Array(text.slice(1,inif)))
				">": #default value
					if ifleft<ifright:
						found=true
						loop(Array(text.slice(1,inif)))
				"in":
					if ifleft in ifright:
						found=true
						loop(Array(text.slice(1,inif)))
				"!=":
					if ifleft != ifright:
						found=true
						loop(Array(text.slice(1,inif)))
				_:
					pass
			
			if not found:
				#print("fail")
				if nextif>1:
					_if(Array(text.slice(nextif,-1)),found)
				
				elif text.size()>1:
					loop(Array(text.slice(inif+1,-1)))
		elif nextif>1:
			#print("elif&else")
			_if(Array(text.slice(nextif,-1)),found)
		
	else:
		############################ print to user!!!!!! #############################################
		$ColorRect/CodeOutput.text += "syntax error. Are you using a conditional argument and ending the if declaration with a colon?\n"
	
	return true
	pass
 

func _var(var text):
	
	#add type to the var array
	#add in variables being set to other variables values.
	var type = ""
	var found =false
	var increment = false
	var decrement = false
	var divide = false
	var multiply = false
	
	var variable = String(Array(text[0]).slice(1,-1))
	print(variable)
	if '+=' in variable:
		variable=variable.split('+=')
		increment = true
	elif '-=' in variable:
		variable=variable.split('-=')
		decrement = true
	elif '*=' in variable:
		variable=variable.split('*=')
		multiply = true
	elif '/=' in variable:
		variable=variable.split('/=')
		divide = true
	elif '=' in variable:
		variable=variable.split('=')
		
	else:
		return false
	
	var tosend=[]
	
	variable=Array(variable)
	#variable name
	variable[0][0]=""
	variable[-1][-1]=""
	
	tosend.append(variable[0].replace(",","").replace(" ",""))
	#variable value if it is equal to another variable.
	#remove extra bracket
	#print(variable)
	
	
	#checking the type implied by the user
	#array
	if "[" and "]" in variable[1]: 
		variable[1][-1]=""
		variable[1][0]=""
		type = "array1"
		
		#while not end of file:
		for i in variable[1]:
			variable[1].strip_edges(true,true)
			i.strip_edges(true,true)
		
		
		var arrayvar = String(variable[1]).replace(" ","").replace(",,",",").replace(",,",",").replace(" ]","]").replace("] ","]").replace(" [","[").replace("[ ","[").replace("],[","¬").replace("],","¬").replace(",[","¬").replace("[","").replace("]","").replace(",¬,","¬")
		arrayvar = Array(arrayvar.split("¬",false))
		
		var count=0
		
		for i in arrayvar:
			if arrayvar.size()==1:
				i=i.strip_edges()
				arrayvar=Array(i.split(",",false))
				break
			else:
				i=Array(i.split(",",false))
				type="array2"
			
		#after deciding whether an array is 1d or 2d we then split it appropriately.
		var todelete =[]
		if type == "array1":
			for i in range(arrayvar.size()):
				if arrayvar[i] == "" or arrayvar[i]==" ":
					todelete.append(i)
				else:
					arrayvar[i]=arrayvar[i].strip_edges()
					arrayvar[i]=arrayvar[i].replace(",","").replace(" ","").strip_edges()
			count=0
			for i in todelete:
				arrayvar.remove(i-count)
				count+=1
		elif type == "array2":
			var outcount =0
			for x in arrayvar:
				count=0
				x=Array(x.split(","))
				for i in range(x.size()):
					if x[i]=="" or x[i]==" ":
						todelete.append(i)
					else:
						arrayvar[outcount][i]=arrayvar[outcount][i].strip_edges()
						arrayvar[outcount][i]=arrayvar[outcount][i].replace(" ","").strip_edges()
				for i in todelete:
					x.remove(i-count)
					count+=1
				outcount+=1
		Array(variable)[1]=arrayvar
		
	#string
	elif '"' in variable[1] or "'" in variable[1]:
		type = "string"
		variable[1]=String(variable[1])
	
	#boolean
	elif Array(variable)[1] in ["True","False"]:
		type = "bool"
		variable[1]=bool(variable[1])
	
	#number
	elif int(variable[1])%1==0:
		type = "number"
		for x in global.vars:
			#print("var:",x)
			print(variable)
			if x[0]==variable[-1]:
				#print("foundit",x)
				var newVar=[]
				newVar.append(variable[0])
				newVar.append(x[1])
				variable=newVar
				break
		if typeof(variable[1])==4:
			variable[1]=float(variable[-1].replace(",","").replace(" ",""))
	
	#everything else
	else:
		type="undefined"
	
	#if rightside is a variable name then set it to the variables value
	
	for i in global.vars:
		if i[0] == variable[0]: 
			found=true;
			if increment == true:
				#if int then do int instead
				if int(i[1])%1==0 and type=="number":
					i[1]=float(i[1])+float(variable[1])
				elif type =="string":
					i[1]+=variable[1]
				elif type=="array":
					i[1].append(variable[1])
				
			elif multiply == true and type == "number":
				i[1]=float(i[1])*float(variable[1])
			elif divide == true and type=="number":
				i[1]=float(i[1])/float(variable[1])
			elif decrement == true and type=="number":
				i[1]=float(i[1])-float(variable[1])
			else:
				i[1]=variable[1]
			break
	
	if variable.size()>=1:
		tosend.append(variable[1])
	else:
		tosend.append(0)
	
	tosend.append(type)
	#only iterate if there is more elements
	if found != true:
		global.vars.append(tosend)
	if text.size()>1:
		loop(Array(text.slice(1,-1)))
	pass 


func _print(var text):
	# print to textlabel.
	var toprint = Array(String(Array(text[0]).slice(1,-1)).replace(",","").replace("(","").replace("[","").replace("]","").replace(")","").replace("print","").split(" "))
	
	for x in toprint.size():
		for i in global.vars:
			print(toprint[x],i)
			print(toprint[x])
			#if the string we're currently viewing isn't a string go to next loop
			if typeof(toprint[x])!=4:
				continue
			if i[0] == toprint[x]:
				#thesame
				toprint[x]=i[1]
				break
				
	toprint=String(toprint)#.replace("[","").replace("]","").replace(",","")
	
	$ColorRect/CodeOutput.text += str(toprint)+"\n"
	if text.size()>1:
		loop(Array(text.slice(1,-1)))
	pass


#takes full string and finds the keywords.
func loop(words):
	
	#shouldn't be called with empty words but just incase we tell the user we have completed the compilation.
	if words.size()<=0:
		$ColorRect/CodeOutput.text += "complete\n"
		return
	
	#ensures that we don't attempt to compare an empty string with the folowing stage.
	if words[0][1] == "":
		loop(Array(words.slice(1,-1)))
	
	var response
	#check for keywords
	match words[0][1]:
		"for":
			response=_for(words)
		"while":
			response=_while(words)
		"if":
			response=_if(words,false)
		"print":
			response=_print(words)
		_: #default value
			response=_var(words)

########################################################################
	#if response == false:
	#	#return falsehoods?
	#	pass

#cleans up initial input and formats it for the rest of the file
func parser(var i):
	#strip_edges takes out whitespace at start and end.
	i=i.strip_edges() 
	
	#separate by line
	var line=i.split("\n")
	var words = Array()
	var count
	for item in line:
		count=0
		
		#swap quadruple spaces to tabs because that is correct way to do it
		item=item.replace("    ","\t")
		
		#loop over length of item we will find how many tabs are there before a real character appears
		for x in item:
			if  x=="\t":
				count+=1
			else:
				#will break at the start of actual code so will not count any tabs if placed at the end of a line.
				break 
		
		#separate each word out. and skip over empty lines.
		item=item.strip_edges() 
		if item=="":
			continue
		
		#ensures that brackets are separated from the functions before starting the next step
		item=item.replace("while(","while (").replace("print(","print (").replace("for(","for (").replace("if(","if (").replace("   "," ").replace("  "," ")
		
		#only 
		var input = String(count)+" "+String(item)
		words.append(input.split(" "))
	loop(Array(words))
	#give words array to be passed down and down and down.
	#each step needs to read from the top line and pass subsequent lines on to a new function.
	pass

#starts the parser and loop functions and then checks world vars to see if correct answer has been submitted.
func _on_code_entered():
	global.vars = []
	var textInput = $CodeText.text
	$ColorRect/CodeOutput.text = ""
	#recurssively bring out the keywords to act as code.
	parser(textInput)
	
	print(global.vars)
	
	var type = global.answer[0]
	#check answer
	for i in global.vars:
		$ColorRect/CodeOutput.text += "checking answer...\n"
		if i[0]==global.answer[0] and String(i[1])==String(global.answer[1]):
			$ColorRect/CodeOutput.text += "You're correct!"
			global.correct=[type,true]
			break
		else:
			global.correct=[type,false]
	
	# tell user if they're correct or not
	# starts timer to know when to hide the face
	$faceTimer.start()
	if global.correct[1]==true:
		$Happy.show()
	else:
		$Sad.show()
	

#once the timer ends hide face
func _on_faceTimer_timeout():
	$Happy.hide()
	$Sad.hide()
	if global.correct[1]:
		global.disabled=false
		self.visible=false
		
	pass

#exit terminal menu.
func _on_Button_pressed():
	global.disabled=false
	self.visible=false
	global.writing=false
	pass 
