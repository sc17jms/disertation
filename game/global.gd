extends Node

#default value for the variable array
var vars = [["ADMININPUTCOMMANDLINENUMBERNOBODYWILLEVERGUESSORUSE",2]] # will be appended with values of variable name and variable value

#default layout for the success criteria
var answer = ["",""]

var gravity=200;
#correct will take the type of answer that was answered and then use the type data to perform some type of winning animation.
#alter gravity, bridge grow, door open, cog turn, that sort of thing.
var correct =["type",false]

#used to disable the character movement when HUD is showing.
var disabled=false

#used to tell the pause menu if the text editor is open and vice versa
var writing=false

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
